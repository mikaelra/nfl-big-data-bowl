cleaning.pynb readme

Cleaned GameWeather, WindSpeed, WindDirectiond, Turf, Location and StadiumType. Fixed typos, generalized some features, espessialy GameWeather. Converted PlayerHeight to inches.

There are a few different team abbreviations used in data. I unifyed them.

We did not clean PlayerCollegeName bc it seemed very irrelevant.

Replaced blanks in OffenseFormation, StadiumType, GameWeather and WindDirection with 'unknown'.

Replaced blanks in FieldPosition with 'centre' bc it's left empty when the line of scrimmage is at the centre of the field.

There are still a few blanks in Orientation, Dir and DefendersInTheBox. And a lot of blanks in Temperature, Humidity, and WindSpeed. 

For modeling and data analysing its better when all the games happen from left to right not from left to right and right to left. For that, we created new coordinates 'NewX' and 'NewY', where 'NewX'='X'-10 and 'NewY'='Y' when play is happening from left to right and 120-X-10 and 160/3-Y when play is happening from right to left. We substraced 10 bc otherwise yardline and x,y-coordinates are not in the same coordinate system. We also made new features 'NewYardLine' which changes from 0-100, compared to YardLine wich changes from 0 to 50. Also made new features 'NewOrientation' and 'NewDir' which are changed 180 degrees from 'Orientation' and 'Dir' when play is happening from right to left. And made new feature 'NewWindDirectrion' which we got from 'WindDirection' after changing directions when play is happening from right to left.

We reated a new CSV file with cleaned data to use in subsequent exploration.

Please note that the notebook was edited in kaggle. Thus, the first two cells where the dataset is imported may have to be altered to fit the requirements of the system you are going to run the file in.