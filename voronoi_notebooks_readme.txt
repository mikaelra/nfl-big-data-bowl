voronoi_notebooks_readme

Got inspiration from:

https://www.kaggle.com/cpmpml/initial-wrangling-voronoi-areas-in-python

https://www.kaggle.com/robikscube/nfl-big-data-bowl-plotting-player-position

Experimented with voronoi areas. Used a sample set of 1000 plays to try out different ways to make the model. Couln't use whole dataset, bc making the plots takes lot of computational power and is very slow. All the experimenting happenend in notebooks "Voronoi and plotting players", "Voronoi2", "Voronoi4", "Voronoi6". And during that, datasets "Voronoi1000", "Voronoi3", "Interaction_voronoi", "Interaction_voronoi2" were made.

Players Voronoi cell sizes depends from which side of the field the play is happening. To reduce the variability we made the voronoi area in the way that line of scimmage would always be in the center and every player would be inside the area and at least 10 yards away from edge, measured along x-axis.

We tried 3 different models:

We predicted where players would be after 0.5 and 1 second when we dont take into account that opposite team members try to block each other. So we just predicted their location taking acount speed, acceleration and running direction.

We predicted where players would be after one second when we take into acount that oposite team players will block each other movements. Used the same features as in previous models, but added that when opposite team members get closer than 0.5 yards to each other, then they dont move anymore.

The main idea was that if the rushing players voronoi area is bigger, then he gains more yards.

Then we investigated how good the models were. For that we made natobook "Voronoi_exploring" where we just visualised different models. Best model seemed to be the one that takes interaction into acount. We then used this model in whole dataset to predict the new coordinates "Interaction_X", "Interaction_Y" and cell size for each player "CellSize". Feature "TotalCellSize" marks the total voronoi area of the play and "CellSizePercent" is equal to "CellSize"/"TotalCellSize". Output dataset was "train_voronoi" and notebook used for all that was "Last_voronoi".

Please note that the notebooks mentioned were edited in kaggle. Thus, the first two cells where the dataset is imported may have to be altered to fit the requirements of the system you are going to run the file in.