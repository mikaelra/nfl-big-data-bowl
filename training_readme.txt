training_readme

We decided to separate training different models into three different notebooks for the sake of convenience. For each model we used the same testing criteria, though the testing function implementations are a little bit different. We measured the absolute error for each prediction as well as for the conventional metric (yards per carry). We then calculated how big was the share of predictions that were more accurate than the standard metric.

In the Random Forest Regressor model we treated yards per carry as a continuous parameter. For LGB and KNN classifiers we had to create 114 discrete classes (1 yard = 1 class).

Please note that each of the training notebooks contains a cell for parameter tuning. These cells take a lot of time to run, however, we edited the notebooks in a way that would make it easy to skip them. 

